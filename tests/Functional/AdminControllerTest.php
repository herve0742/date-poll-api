<?php

namespace App\Tests\Functional;

use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AdminControllerTest extends WebTestCase {
  use FixturesTrait;

  // Test getting all polls
  public function testGetAllPolls() {
    $client = static::createClient();

    $this->loadFixtures(array(
        'App\DataFixtures\AppPollFixtures'
    ));

    $client->request('GET', '/admin/polls/clean/testAdminToken');

    $response = $client->getResponse();
    $this->assertEquals(200, $response->getStatusCode());
    $body = $response->getContent();
    $json = json_decode($body, true);
    $this->assertEquals(4, $json['data']['count']);

    //This call is supposed to be nilpotent
    $client->request('GET', '/admin/polls/clean/testAdminToken');
    $response = $client->getResponse();
    $json = json_decode($response->getContent(), true);
    $this->assertEquals(0, $json['data']['count']);

  }

}
