# Installer PHP-8

D'abord il vous faut vous débarasser des versions précédentes de php (si vous ne lancez pas ce projet à partir de docker).
```bash
sudo apt purge php-common
```

Pour Ubuntu / Debian, utilisez le ppa suivant. Mettez à jour Apt, puis installez php et hop, vous avez la version 8.
```bash
sudo add-apt-repository ppa:ondrej/php
sudo apt install php8.0 php8.0-fpm php8.0-mysql php8.0-bcrypt nginx

```
