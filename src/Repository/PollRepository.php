<?php

namespace App\Repository;

use App\Entity\Poll;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Poll|null find( $id, $lockMode = null, $lockVersion = null )
 * @method Poll|null findOneBy( array $criteria, array $orderBy = null )
 * @method Poll[]    findAll()
 * @method Poll[]    findBy( array $criteria, array $orderBy = null, $limit = null, $offset = null )
 */
class PollRepository extends ServiceEntityRepository {
	public function __construct(
		ManagerRegistry $registry
	) {
		parent::__construct( $registry, Poll::class );
	}

	// /**
	//  * @return Poll[] Returns an array of Poll objects
	//  */

	public function findExpiredPolls() {
		return $this->findExpirationPollOfDay( 0 );
	}

	public function findSoonExpiredPolls() {
		return $this->findExpirationPollOfDay( 30 );
	}

	public function findDeletableExpiredPolls() {
		return $this->findExpirationPollOfDay( - 30 );
	}

	public function findExpirationPollOfDay( $count_of_days ) {

		$today = new \DateTime();

		if ( $count_of_days > - 1 ) {

			$date_soon = $today->add( new \DateInterval( 'P' . $count_of_days . 'D' ) );
		} else {
			$date_soon = $today->sub( new \DateInterval( 'P' . abs( $count_of_days ) . 'D' ) );

		}

		return $this->createQueryBuilder( 'p' )
		            ->andWhere( 'p.expiracyDate < :date_soon' )
		            ->setParameter( 'date_soon', $date_soon )
		            ->orderBy( 'p.id', 'ASC' )
		            ->getQuery()
		            ->getResult();
	}

	/**
	 * find all the polls of an owner
	 *
	 * @param $email
	 *
	 * @return int|mixed|string
	 */
	public function findAllByOwnerEmail( $email ) {
		return $this->createQueryBuilder( 'p' )
		            ->andWhere( 'p.owner.email = :email' )
		            ->setParameter( 'email', $email )
		            ->orderBy( 'p.id', 'DESC' )
		            ->getQuery()
		            ->getResult();
	}


}
