<?php

namespace App\Traits;

use DateInterval;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

trait TimeStampableTrait {
	/**
	 * @ORM\Column(type="datetime" , options={"default"="CURRENT_TIMESTAMP"})
	 */
	private $createdAt;

	public function getCreatedAt(): ?DateTimeInterface {
		return $this->createdAt;
	}

	public function setCreatedAt( DateTimeInterface $createdAt ): self {
		$this->createdAt = $createdAt;

		return $this;
	}

	public function getCreatedAtAsString(): string {
		return $this->createdAt->format( 'c' );
	}


	public function addDaysToDate( DateTime $date, int $days ) {

		return $date->add( new DateInterval( 'P' . $days . 'D' ) );
	}


}
