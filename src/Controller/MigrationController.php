<?php

namespace App\Controller;

//use FOS\RestBundle\Controller\Annotations\Get;
//use FOS\RestBundle\Controller\Annotations\Route;
use App\Entity\Choice;
use App\Entity\Comment;
use App\Entity\Owner;
use App\Entity\Poll;
use App\Entity\StackOfVotes;
use App\Entity\Vote;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Route;
use PDO;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class DefaultController
 * @package App\Controller
 * @Route("/migration-from-v1",name="admin_homepage")
 */
class MigrationController extends EmailsController {
	/**
	 * @Get(path ="/{unique_key}",
	 *      name = "_migrate_from_v1")
	 */
	public function indexAction( $unique_key ) {

		// get env vars
		// check uniq key is good
		if ( $unique_key !== $this->getParameter( 'UNIQ_INSTALL_KEY' ) ) {
			return new JsonResponse( [
				'error' => 'NOPE! veuillez vérifier votre fichier .env',
			] );
		}


		// fetch old Database

		$debug = '';

		$em          = $this->getDoctrine()->getManager();
		$pollsBySlug = [];


		$pdo_options[ PDO::ATTR_ERRMODE ] = PDO::ERRMODE_EXCEPTION;
		$bdd                              = new PDO( 'mysql:host=localhost;dbname=' . $this->getParameter( 'OLD_DATABASE_NAME' ),
			$this->getParameter( 'OLD_DATABASE_USER' ),
			$this->getParameter( 'OLD_DATABASE_PASS' ),
			$pdo_options );
		$res_polls                        = $bdd->query( 'SELECT * FROM fd_poll' );
		while ( $d = $res_polls->fetch( PDO::FETCH_OBJ ) ) {

			$debug .= " <br> ajout de sondage : " . $d->title . '  - ' . $d->id;


			$newPoll = new Poll();
			$owner   = new Owner();

			$owner->setEmail( $d->admin_mail )
			      ->setPseudo( $d->admin_name )
			      ->addPoll( $newPoll );

			$newPoll
				->setOwner( $owner )
				->setCustomURL( $d->id )
				->setKind( $d->id === 'D' ? 'date' : 'text' )
				->setHideResults( ! $d->results_publicly_visible )
				->setAdminKey( $d->admin_id )
				->setTitle( $d->title )
				->setVotesAllowed( $d->receiveNewVotes )
				->setCommentsAllowed( $d->receiveNewComments )
				->setChoicesMax( $d->ValueMax )
				->setPassword( $d->password_hash )
				->setDescription( $d->description )
				->setCreatedAt( date_create( $d->creation_date ) );

			$pollsBySlug[ $d->id ] = $newPoll;


			$em->persist( $owner );
			$em->persist( $newPoll );
		}
		// get choices, slots and link them with poll by their slug
		$res_slots                = $bdd->query( 'SELECT * FROM fd_slot' );
		$pollChoicesOrderedBySlug = [];
		$choicesCreated           = [];

		while ( $d = $res_slots->fetch( PDO::FETCH_OBJ ) ) {

			$debug .= " <br> ajout de slot, converti en choix de réponse : " . $d->poll_id . ' : ' . $d->moments;

			$pollSlug = $d->poll_id;
			$poll     = $pollsBySlug[ $pollSlug ];

			$moments = explode( ',', $d->moments );
			foreach ( $moments as $moment ) {
				$newChoice = new Choice();
				$dateOfDay = date_create( strtotime( $d->title ) );
				$newChoice
					->setPoll( $poll )
					->setName( $dateOfDay->format('y-m-d'). ' >>> ' . $moment );

				$pollChoicesOrderedBySlug[ $pollSlug ][] = $newChoice;
				$poll->addChoice( $newChoice );

				$em->persist( $newChoice );
				$em->persist( $newPoll );
				$choicesCreated[] = $newChoice;
			}
		}

		// get votes
		$stacksOfVote = [];
		$res_votes    = $bdd->query( 'SELECT * FROM fd_vote' );
		while ( $d = $res_votes->fetch( PDO::FETCH_OBJ ) ) {

			$debug    .= " <br> ajout de stack de vote : " . $d->name;
			$pollSlug = $d->poll_id;
			$poll     = $pollsBySlug[ $pollSlug ];

			$newStack = new StackOfVotes();
			$newOwner = new Owner();
			$newOwner
				->setPseudo( $d->name )
				->setEmail( 'the_anonymous_email_from_@_migration_offramadate.org' )
				->setModifierToken( $d->uniqId );

			$newStack->setPoll( $poll )
			         ->setOwner( $newOwner )
			         ->setPseudo( $d->name );

			// each choice answer is encoded in a value :

			$voteCodes = str_split( $d->choices );
			// get choices of the poll and answer accordingly

			$ii = 0;
			foreach ( $voteCodes as $vote_code ) {
				if ( $vote_code !== ' ' ) {
					$choice = $pollChoicesOrderedBySlug[ $pollSlug ][ $ii ];

					$newVote = new Vote();

					$newVote
						->setChoice( $choice )
						->setStacksOfVotes( $newStack )
						->setPoll( $poll )
						->setValue( $this->mapAnswerNumberToWord( $vote_code ) );
					$newStack->addVote( $newVote );

					$em->persist( $newVote );
					$votes[] = $newVote;
				}
				$ii ++;
			}


			$poll->addStackOfVote( $newStack );
			$em->persist( $newStack );
			$stacksOfVote[] = $newStack;

		}

		$comments     = [];
		$res_comments = $bdd->query( 'SELECT * FROM fd_comment' );
		while ( $d = $res_comments->fetch( PDO::FETCH_OBJ ) ) {

			$debug .= " <br> ajout de commentaire : " . $d->name . ' ' . $d->comment;

			$pollSlug   = $d->poll_id;
			$poll       = $pollsBySlug[ $pollSlug ];
			$newComment = new Comment();
			$poll->addComment( $newComment );

			$newComment->setPoll( $poll )
			           ->setCreatedAt( date_create( $d->date ) )
			           ->setText( $d->comment )
				// TODO update entities
				       ->setPseudo( $d->name );
			$em->persist( $newComment );
			$comments[] = $newComment;

		}
		// gather objects
		// create new polls
		$em->flush();
		// success
		// failure notice
		$debug .= " <br> <br> ça c'est fait. ";

		return $this->render( 'pages/migration.html.twig',
			[
				"message"           => "welcome to the framadate migration endpoint, it has yet to be done",
				"debug"             => $debug,
				"OLD_DATABASE_NAME" => $this->getParameter( 'OLD_DATABASE_NAME' ),
				"OLD_DATABASE_USER" => $this->getParameter( 'OLD_DATABASE_USER' ),
				"counters"          => [
					'polls'           => count( $pollsBySlug ),
					'comments'        => count( $comments ),
					'choices'         => count( $choicesCreated ),
					'stacks_of_votes' => count( $stacksOfVote ),
					'votes'           => count( $votes ),
				],
			] );
	}

	/**
	 * @param $numberToConvert
	 * conversion of answer:
	 * space character : no answer, 0 : no , 1 : maybe , 2 : yes
	 *
	 * @return string
	 */
	public function mapAnswerNumberToWord( $numberToConvert ) {
		$word = '';
		switch ( $numberToConvert ) {
			case 0:
				$word = 'no';
				break;
			case 1:
				$word = 'maybe';
				break;
			case 2:
				$word = 'yes';
				break;
			default:
				$word = 'no';
		}

		return $word;
	}

}
