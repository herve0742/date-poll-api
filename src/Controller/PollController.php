<?php

namespace App\Controller;

use App\Entity\Poll;
use App\Form\PollType;
use App\Repository\PollRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/poll")
 */
class PollController extends AbstractController {
	/**
	 * @Route("/", name="poll_index", methods={"GET"})
	 */
	public function index( PollRepository $pollRepository ): Response {

		$polls  = $pollRepository->findAll();
		$titles = [];
		foreach ( $polls as $poll ) {
			$titles[] = $poll->getTitle();
		}

		return $this->render( 'poll/index.html.twig',
			[
				'count'  => count( $polls ),
				'titles' => $titles,
				'polls'  => $polls,
			] );
	}

	/**
	 * @Route("/new", name="poll_new_old", methods={"POST"})
	 */
	public function new( Request $request ): Response {
		$poll = new Poll();
		$form = $this->createForm( PollType::class, $poll );
		$form->handleRequest( $request );

		if ( $form->isSubmitted() && $form->isValid() ) {
			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->persist( $poll );
			$entityManager->flush();

			return $this->redirectToRoute( 'poll_index' );
		}

		return $this->render( 'poll/new.html.twig',
			[
				'poll' => $poll,
				'form' => $form->createView(),
			] );
	}

	/**
	 * on cherche un sondage par son url personnalisée
	 * @Route("/id/{id}", name="poll_show", methods={"GET"})
	 */
	public function show( $id ): Response {
		$repository = $this->getDoctrine()->getRepository( Poll::class );
		$foundPoll  = $repository->findOneByCustomUrl( $id );
		if ( ! $foundPoll ) {
			return $this->json( [
				'message' => $id . ' : not found',
			],
				404 );
		}

		return $this->render( 'poll/show.html.twig',
			[
				'poll' => $foundPoll,
			] );
	}

	/**
	 * @Route("/{id}/edit", name="poll_edit", methods={"GET","POST"})
	 */
	public function edit( Request $request, Poll $poll ): Response {
		$form = $this->createForm( PollType::class, $poll );
		$form->handleRequest( $request );

		if ( $form->isSubmitted() && $form->isValid() ) {
			$this->getDoctrine()->getManager()->flush();

			return $this->redirectToRoute( 'poll_index' );
		}

		return $this->render( 'poll/edit.html.twig',
			[
				'poll' => $poll,
				'form' => $form->createView(),
			] );
	}

	/**
	 * @Route("/{id}", name="poll_delete", methods={"DELETE"})
	 */
	public function delete( Request $request, Poll $poll ): Response {
		if ( $this->isCsrfTokenValid( 'delete' . $poll->getId(), $request->request->get( '_token' ) ) ) {
			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->remove( $poll );
			$entityManager->flush();
		}

		return $this->redirectToRoute( 'poll_index' );
	}
}
