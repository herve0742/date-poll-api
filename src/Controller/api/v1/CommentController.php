<?php

namespace App\Controller\api\v1;

use App\Controller\EmailsController;
use App\Entity\Comment;
use App\Entity\Owner;
use App\Entity\Poll;
use DateTime;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Route;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 * @package App\Controller
 * @Route("/api/v1/comment",
 *     name="api_comment_")
 */
class CommentController extends EmailsController
{

    /**
     * @Get(
     *     path = "/poll/{customUrl}",
     *     name = "get_poll_comment"
     * )
     */
    public
    function getPollCommentsAction(
        SerializerInterface $serializer,
        Poll $poll
    )
    {
        $jsonResponse = $serializer->serialize([
            'message' => 'here are your comments of the poll',
            'data' => $poll->getComments(),
        ],
            'json');

        $response = new Response($jsonResponse);
        $response->headers->set('Content-Type', 'application/json');
        $response->setStatusCode(200);

        return $response;

    }

    /**
     * add a comment on a poll
     * @Route(
     *     "/poll/{customUrl}",
     *     "_new_comment",
     *     methods={"POST"}
     * )
     */
    public
    function newCommentAction(
        Poll $poll,
        Request $request
    )
    {

        $data = json_decode($request->getContent(), true);
//        return $this->json(['pseudo' => $data["pseudo"]], 404);

        if (!$poll) {
            return $this->json(['message' => 'poll not found'], 404);
        }

        $comment = new Comment();
        $owner = new Owner();
        $owner
            ->setPseudo($data["pseudo"])
            ->setEmail('anonymous@example.com');

        $comment->setOwner($owner)
            ->setPseudo($data["pseudo"])
            ->setText($data["text"])
            ->setCreatedAt(new DateTime())
            ->setPoll($poll);
        $owner
            ->addComment($comment);

        $em = $this->getDoctrine()->getManager();
        $em->persist($owner);
        $em->persist($comment);
        $em->flush();

        if ($poll->getMailOnComment()) {
            $this->sendCommentNotificationAction($owner, $comment);
        }


        return $this->json( $comment->display(),


            201);

    }

    /**
     * add a comment on a poll
     * @Route(
     *     "/poll/{customUrl}/by-owner",
     *     "_new_comment_by_owner",
     *     methods={"POST"}
     * )
     */
    public
    function newCommentByOwnerAction(
        Poll $poll,
        Request $request
    )
    {
        if (!$poll) {
            return $this->json(['message' => 'poll not found'], 404);
        }
        $data = $request->getContent();

        $serializer = SerializerBuilder::create()->build();
        $comment = $serializer->deserialize($data, 'App\Entity\Comment', 'json');

        $em = $this->getDoctrine()->getRepository(Owner::class);

        $data = json_decode($data, true);
        if (!isset($data['email'])) {
            return $this->json(["message" => "Incorrect JSON in request"], 400);
        }

        $foundOwner = $em->findOneByEmail($data['email']);
        // manage existing or new Owner
        if (!$foundOwner) {
            $foundOwner = new Owner();
            $foundOwner->setPseudo($data['pseudo'])
                ->setEmail($data['email'])
                ->setModifierToken(uniqid('', true));
        }
        // anti flood
        $seconds_limit_lastpost = 5;
        $emComment = $this->getDoctrine()->getRepository(Comment::class);
        $lastCommentOfOwner = $emComment->findBy(['owner' => $foundOwner], ['id' => 'desc']);

        // TODO anti flood by session / IP

        if ($lastCommentOfOwner) {


            // check time of last comment
            $now = new DateTime();
            $now = $now->format('Y-m-d H:i:s');
            $date_first = strtotime($lastCommentOfOwner[0]->getCreatedAt()->format('Y-m-d H:i:s'));
            $date_second = strtotime($now);

            if (($date_second - $date_first) < $seconds_limit_lastpost) {
                return $this->json([
                    'message' => 'anti flood déclenché',
                    'details' => 'votre deriner commentaire a été envoyé il y a moins de ' . $seconds_limit_lastpost . ' secondes',
                ],
                    403);
            }

            // check similar text content
            if ($lastCommentOfOwner[0]->getText() == $comment->getText()) {
                return $this->json([
                    'message' => 'anti flood déclenché',
                    'details' => 'votre deriner commentaire a exactement le même contenu que celui ci, il n\'a donc pas été créé',
                ],
                    403);
            }
        }
        $comment->setOwner($foundOwner)
            ->setCreatedAt(new DateTime())
            ->setPoll($poll);
        $foundOwner->addComment($comment);

        $em = $this->getDoctrine()->getManager();
        $em->persist($foundOwner);
        $em->persist($comment);
        $em->flush();

        if ($poll->getMailOnComment()) {
            $this->sendCommentNotificationAction($foundOwner, $comment);
        }


        return $this->json([
            'message' => 'you created a comment',
            'data' => [
                'your_comment' => $comment->display(),
            ],
        ],
            201);
    }

    /**
     * Erase all comments of a poll
     * @Delete(
     *     path = "/poll/{customUrl}",
     *     name = "poll_comments_delete"
     * )
     *
     * @param Poll $poll
     * @param $accessToken
     *
     * @return JsonResponse
     */
    public
    function deletePollCommentsAction(
        Poll $poll,
        $accessToken
    )
    {
        if ($accessToken == $poll->getAdminKey()) {
            $em = $this->getDoctrine()->getManager();
            $length = count($poll->getComments());
            $em->remove($poll->getComments());
            $em->flush();

            return $this->json([
                'message' => 'boom! les ' . $length . ' commentaires du sondage ont été supprimés',
            ]);
        } else {
            return $this->json([
                'message' => 'le token d\'autorisation est invalide, vous ne pouvez pas modifier ce sondage',
            ]);
        }
    }

}
