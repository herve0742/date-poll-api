<?php

namespace App\Controller;

use App\Entity\Poll;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;

/**
 * Class DefaultController
 * @package App\Controller
 * @Route("/",name="home_")
 */
class DefaultController extends EmailsController {

	/**
	 * @Get(path ="/",
	 *      name = "sweet_home")
	 */
	public function indexAction() {

		$polls = $this->getDoctrine()->getRepository( Poll::class )->findAll();

		return $this->render( 'pages/home.html.twig', [] );
	}


}
