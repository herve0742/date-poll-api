<?php

namespace App\Entity;

use App\Traits\TimeStampableTrait;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * one poll choice, could be a text or a date
 * @ORM\Entity(repositoryClass="App\Repository\ChoiceRepository")
 * @Serializer\ExclusionPolicy("all")
 */
class Choice {

	use TimeStampableTrait;

	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 * @Serializer\Expose()
	 */
	public $id;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 * @Serializer\Type("string")
	 * @Serializer\Expose()
	 */
	public $name;
	/**
	 * @ORM\Column(type="string", length=1024, nullable=true)
	 * @Serializer\Type("string")
	 * @Serializer\Expose()
	 */
	public $url;


	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\Vote", mappedBy="choice", cascade={"persist"})
	 * @Serializer\Type("App\Entity\Vote")
	 */
	public $votes;
	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Poll", inversedBy="choices", cascade={"persist"})
	 * @Serializer\Type("App\Entity\Poll")
	 */
	private $poll;

	public function __construct( $optionalName = null ) {
		$this->setCreatedAt( new DateTime() );
		$this->poll  = new ArrayCollection();
		$this->votes = new ArrayCollection();
		if ( $optionalName ) {
			$this->setName( $optionalName );
		}
	}

	public function display( $kind = 'text' ): array {
		if($kind == 'text'){
			$fields = [
				'id'         => $this->getId(),
				'created_at' => $this->getCreatedAtAsString(),
				'name'       => $this->getName(),
				'url'        => $this->getUrl(),
			];
		}
		elseif($kind=='date'){
			$fields = [
				'id'         => $this->getId(),
				'created_at' => $this->getCreatedAtAsString(),
				'name'       => $this->getName(),
//				'url'        => $this->getUrl(),
			];
		}

		return $fields;
	}

	public function getId(): ?int {
		return $this->id;
	}

	public function getName(): ?string {
		return $this->name;
	}

	public function setName( ?string $name ): self {
		$this->name = $name;

		return $this;
	}

	public function getUrl(): ?string {
		return $this->url;
	}

	public function setUrl( ?string $url ): self {
		$this->url = $url;

		return $this;
	}

	public function getPoll(): ?ArrayCollection {
		return $this->poll;
	}

	public function setPoll( ?Poll $poll ): self {
		$this->poll = $poll;

		return $this;
	}

	/**
	 * @return Collection|Vote[]
	 */
	public function getVotes(): Collection {
		return $this->votes;
	}

	public function addVote( Vote $vote ): self {
		if ( ! $this->votes->contains( $vote ) ) {
			$this->votes[] = $vote;
			$vote->setChoice( $this );
		}

		return $this;
	}

	public function removeVote( Vote $vote ): self {
		if ( $this->votes->contains( $vote ) ) {
			$this->votes->removeElement( $vote );
			// set the owning side to null (unless already changed)
			if ( $vote->getChoice() === $this ) {
				$vote->setChoice( null );
			}
		}

		return $this;
	}
}
