<?php

namespace App\DataFixtures;

use App\Entity\Choice;
use App\Entity\Comment;
use App\Entity\Owner;
use App\Entity\Poll;
use App\Entity\StackOfVotes;
use App\Entity\Vote;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppPollFixtures extends Fixture {
	public const POLL_FIXTURE_ONE = 'citron-poll-fixture';
	public const POLL_FIXTURE_TWO = 'aujourdhui-ou-demain';
	public const POLL_FIXTURE_THREE = 'citron';
	public const POLL_FIXTURE_FOUR = 'demo';

	public function load( ObjectManager $manager ) {

		/**
		 * create a few demo people
		 */
		$owner = new Owner(); // someone who creates the polls
		$owner->setEmail( 'tktest@tktest.com' )
		      ->setPseudo( 'tk_TEST' );
		$commenterMan = new Owner();
		$commenterMan->setEmail( 'tktest_commentateur@tktest.com' )
		             ->setPseudo( 'tk_TEST_commentateur' );

		$voter = new Owner();
		$voter->setEmail( 'testing_vote_people@tktest.com' )
		      ->setPseudo( 'voting_people_TEST' )
		      ->setModifierToken( uniqid() );
		$manager->persist( $owner );
		$manager->persist( $commenterMan );
		$manager->persist( $voter );
		$manager->flush();


		$pollCitronOrange = new Poll();
		$pollCitronOrange->setTitle( 'citron ou orange' )
		                 ->setCustomUrl( 'citron' )
		                 ->setDescription( 'votre sorbert préféré' )
		                 ->setModificationPolicy( 'nobody' )
		                 ->setPassword( 'le pass woute woute' );

		$this->addReference( self::POLL_FIXTURE_ONE, $pollCitronOrange );

		$pollCitronOrange->setMailOnVote( true );
		$pollCitronOrange->setOwner( $owner );
		$owner->addPoll( $pollCitronOrange );

		$choiceA = new Choice();
		$choiceA->setName( 'citron' );
		$choiceB = new Choice();
		$choiceB->setName( 'orange' );

		$pollCitronOrange
			->addChoice( $choiceA )
			->addChoice( $choiceB );
		$manager->persist( $pollCitronOrange );

		$stack1 = new StackOfVotes();
		$stack1
			->setPoll( $pollCitronOrange )
			->setOwner( $voter );

		$voteA = new Vote();
		$voteA
			->setPoll( $pollCitronOrange )
			->setStacksOfVotes( $stack1 )
			->setValue( "yes" )
			->setChoice( $choiceA );
		$voteB = new Vote();
		$voteB
			->setPoll( $pollCitronOrange )
			->setStacksOfVotes( $stack1 )
			->setValue( "yes" )
			->setChoice( $choiceB );
		$stack1->setPseudo( 'chuck norris' );
		$manager->persist( $stack1 );

		// voter guy votes again with an other pseudo

		$stack2 = new StackOfVotes();
		$stack2->setPseudo( 'Jean indécis' );
		$stack2
			->setPoll( $pollCitronOrange )
			->setOwner( $voter );

		$voteA = new Vote();
		$voteA
			->setPoll( $pollCitronOrange )
			->setStacksOfVotes( $stack2 )
			->setValue( "maybe" )
			->setChoice( $choiceA );

		$manager->persist( $stack2 );


		$pollCitronOrange = new Poll();

		$ownerComment = new Comment();
		$ownerComment
			->setText( "trop bien ce sondage wohooo! signé l'auteur." )
			->setPseudo( 'un gens qui commente' )
			->setOwner( $owner );
		$pollCitronOrange->addComment( $ownerComment );

		$someoneComment = new Comment();
		$someoneComment
			->setText( "comme l'auteur se la raconte. PFFFF!" )
			->setPseudo( 'un gens qui commente' )
			->setOwner( $commenterMan );
		$pollCitronOrange->addComment( $someoneComment );


		$pollCitronOrange->setTitle( 'démo sondage de texte avec deux commentaires' )
		                 ->setCustomUrl( 'demo' )
		                 ->setDescription( 'description du sondage 2' );

		$pollCitronOrange->setModificationPolicy( 'self' );
		$pollCitronOrange->setMailOnComment( true );


		$pollCitronOrange->addTextChoiceArray( [ 'un truc', 'deux trucs' ] );
		$pollCitronOrange->setOwner( $owner );
		$owner->addPoll( $pollCitronOrange );

		$manager->persist( $pollCitronOrange );
		$manager->persist( $someoneComment );
		$manager->persist( $ownerComment );


		// voting test with 2 people

		// poll with date type
		$pollCitronOrange = new Poll();
		$choice           = new Choice();
		$firstDate        = new DateTime();
		$choice->setName( $firstDate->format( 'Y-m-d H:i:s' ) );
		$choice2 = new Choice();
		$choice3 = new Choice();
		$choice2->setName( $pollCitronOrange->addDaysToDate( $firstDate, 1 )->format( 'Y-m-d H:i:s' ) );
		$choice3->setName( $pollCitronOrange->addDaysToDate( $firstDate, 2 )->format( 'Y-m-d H:i:s' ) );

		$pollCitronOrange->setTitle( "c'est pour aujourdhui ou pour demain" )
		                 ->setCustomUrl( 'aujourdhui-ou-demain' )
		                 ->setDescription( 'Vous avez le choix dans la date' )
		                 ->setKind( 'date' )
		                 ->setOwner( $owner )
		                 ->addChoice( $choice )
		                 ->addChoice( $choice2 )
		                 ->addChoice( $choice3 )
		                 ->setModificationPolicy( 'everybody' );
		$manager->persist( $pollCitronOrange );

		// poll with cartoon choices
		$pollCartoons = new Poll();
		$pollCartoons->setTitle( 'dessin animé préféré' )
		             ->setCustomUrl( 'dessin-anime' )
		             ->setDescription( 'choisissez votre animé préféré' )
		             ->setOwner( $owner )
		             ->setModificationPolicy( 'nobody' )
		             ->addTextChoiceArray( [
			             "Vic le viking",
			             "Boumbo petite automobile",
			             "Les mystérieuses cités d'or",
			             "Les mondes engloutis",
			             "Foot 2 rue",
			             "Le chat, la vache, et l'océan",
			             "Digimon",
		             ] );

		$someoneComment = new Comment();
		$someoneComment
			->setPseudo( 'un gens qui commente' )
			->setText( "allez boumbo!" )
			->setOwner( $commenterMan );
		$pollCartoons->addComment( $someoneComment );
		$someoneComment2 = new Comment();
		$someoneComment2
			->setPseudo( 'un gens qui commente' )
			->setText( "je suis pour la team rocket de digimon" )
			->setOwner( $owner );
		$pollCartoons->addComment( $someoneComment2 );

		$manager->persist( $pollCartoons );

		$stack = new StackOfVotes();
		$stack->setPseudo( 'Wulfila' );
		$stack
			->setPoll( $pollCartoons )
			->setOwner( $voter );
		$pollCartoons->addStackOfVote( $stack );

		$vote = new Vote();
		$vote
			->setPoll( $pollCartoons )
			->setStacksOfVotes( $stack )
			->setValue( "yes" )
			->setChoice( $pollCartoons->getChoices()[ 2 ] );
		$pollCartoons->addVote( $vote );

		$vote = new Vote();
		$vote
			->setPoll( $pollCartoons )
			->setStacksOfVotes( $stack )
			->setValue( "maybe" )
			->setChoice( $pollCartoons->getChoices()[ 1 ] );
		$pollCartoons->addVote( $vote );

		$manager->persist( $stack );

		$stack = new StackOfVotes();
		$stack->setPseudo( 'Tykayn' );
		$stack
			->setPoll( $pollCartoons )
			->setOwner( $voter );

		$vote = new Vote();
		$vote
			->setPoll( $pollCartoons )
			->setStacksOfVotes( $stack )
			->setValue( "yes" )
			->setChoice( $pollCartoons->getChoices()[ 1 ] );
		$pollCartoons->addVote( $vote );

		$vote = new Vote();
		$vote
			->setPoll( $pollCartoons )
			->setStacksOfVotes( $stack )
			->setValue( "yes" )
			->setChoice( $pollCartoons->getChoices()[ 2 ] );
		$pollCartoons->addVote( $vote );

		$vote = new Vote();
		$vote
			->setPoll( $pollCartoons )
			->setStacksOfVotes( $stack )
			->setValue( "no" )
			->setChoice( $pollCartoons->getChoices()[ 2 ] );
		$pollCartoons->addVote( $vote );

		$manager->persist( $pollCartoons );
		$manager->persist( $stack );


		$manager->persist( $commenterMan );

		$manager->flush();
	}
}
